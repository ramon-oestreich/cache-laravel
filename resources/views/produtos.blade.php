<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lista</title>
    <style>
    table{
        border-collapse: collapse;
    }
    table, th,td{
        border: 1px solid black;
    }
    </style>
</head>
<body>
    <table>
        <tr>
            <td>ID</td>
            <td>Nome</td>
            <td>Categorias</td>
        </tr>
        @foreach ($produtos as $p)    
         <tr>
            <td>{{$p->id}}</td>
            <td>{{$p->nome}}</td>
            <td>
                <ul>
                @foreach ($p->categorias as $c)
                    <li>{{$c->nome}}</li>
                @endforeach
                </ul>
            </td>
        </tr>
        @endforeach
    </table>
</body>
</html>